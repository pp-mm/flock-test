package com.example.steering;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.steer.behaviors.Alignment;
import com.badlogic.gdx.ai.steer.behaviors.BlendedSteering;
import com.badlogic.gdx.ai.steer.behaviors.Cohesion;
import com.badlogic.gdx.ai.steer.behaviors.PrioritySteering;
import com.badlogic.gdx.ai.steer.behaviors.Separation;
import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.ai.steer.limiters.LinearAccelerationLimiter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.example.steering.behavior.FleeIfWithin;
import com.example.steering.box2d.Box2dRadiusProximity;
import com.example.steering.box2d.Box2dSteeringEntity;

public class Box2dSteering extends ApplicationAdapter {

  private World world;
  private Box2DDebugRenderer renderer;
  private OrthographicCamera camera;
  private Array<Box2dSteeringEntity> characters;

  public Box2dSteeringEntity createSteeringEntity(boolean independentFacing, float x, float y, float radius) {
    CircleShape circleChape = new CircleShape();
    circleChape.setPosition(new Vector2());
    circleChape.setRadius(radius);

    BodyDef characterBodyDef = new BodyDef();
    characterBodyDef.position.set(x, y);
    characterBodyDef.type = BodyDef.BodyType.DynamicBody;
    Body characterBody = world.createBody(characterBodyDef);

    FixtureDef charFixtureDef = new FixtureDef();
    charFixtureDef.density = 1;
    charFixtureDef.shape = circleChape;
    charFixtureDef.filter.groupIndex = 0;
    charFixtureDef.isSensor = true;
    characterBody.createFixture(charFixtureDef);

    circleChape.dispose();

    return new Box2dSteeringEntity(characterBody, independentFacing, radius);
  }

  @Override
  public void create() {
    characters = new Array<Box2dSteeringEntity>();
    world = new World(new Vector2(), false);
    renderer = new Box2DDebugRenderer();
    camera = new OrthographicCamera(60, 40);
    create300FlockingShoal();
  }

  @Override
  public void render() {
    float delta = Gdx.graphics.getDeltaTime();
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    world.step(delta, 8, 3);
    for (Box2dSteeringEntity character : characters) {
      character.update(delta);
    }
    camera.update();
    renderer.render(world, camera.combined);
  }

  private void create300FlockingShoal() {
    Box2dSteeringEntity predator = createSteeringEntity(false, 0, 0, 1f);
    predator.setMaxLinearSpeed(3f);
    predator.setMaxLinearAcceleration(1000);
    Wander<Vector2> predatorWanderSB = new Wander<Vector2>(predator)
      .setFaceEnabled(false)
      .setLimiter(new LinearAccelerationLimiter(1000))
      .setWanderOffset(60)
      .setWanderOrientation(10)
      .setWanderRadius(40)
      .setWanderRate(MathUtils.PI / 5);
    predator.setSteeringBehavior(predatorWanderSB);
    characters.add(predator);
    for (int i = 0; i < 300; i++) {
      float x = MathUtils.random(-30, 30);
      float y = MathUtils.random(-20, 20);
      Box2dSteeringEntity character = createSteeringEntity(false, x, y, 0.25f);
      character.setMaxLinearSpeed(15f);
      // faster response
      character.setMaxLinearAcceleration(1000);

      Box2dRadiusProximity proximity = new Box2dRadiusProximity(character, world, 1);
      Alignment<Vector2> groupAlignmentSB = new Alignment<Vector2>(character, proximity);
      Cohesion<Vector2> groupCohesionSB = new Cohesion<Vector2>(character, proximity);
      Separation<Vector2> groupSeparationSB = new Separation<Vector2>(character, proximity).setDecayCoefficient(500);
      FleeIfWithin<Vector2> fleeSB = new FleeIfWithin<Vector2>(character, predator, 10);
      BlendedSteering<Vector2> blendedSteering = new BlendedSteering<Vector2>(character) //
        .add(groupAlignmentSB, 1f) //
        .add(groupCohesionSB, 1f) //
        .add(groupSeparationSB, 3f);

      Wander<Vector2> wanderSB = new Wander<Vector2>(character)
        .setFaceEnabled(false)
        .setLimiter(new LinearAccelerationLimiter(1000))
        .setWanderOffset(60)
        .setWanderOrientation(10)
        .setWanderRadius(40)
        .setWanderRate(MathUtils.PI / 5);
      
      
      PrioritySteering<Vector2> prioritySteeringSB = new PrioritySteering<Vector2>(character, 0.0001f) //
//        .add(fleeSB)
        .add(blendedSteering) //
        .add(wanderSB);

      character.setSteeringBehavior(prioritySteeringSB);
      characters.add(character);
    }
  }
}
