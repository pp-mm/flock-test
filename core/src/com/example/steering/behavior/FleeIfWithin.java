/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.steering.behavior;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.behaviors.Seek;
import com.badlogic.gdx.math.Vector;

/**
 *
 * @author implicit-invocation
 */
public class FleeIfWithin<T extends Vector<T>> extends Seek<T> {

  private float radius;

  /**
   * Creates a {@code Flee} behavior for the specified owner.
   *
   * @param owner the owner of this behavior.
   * @param radius
   */
  public FleeIfWithin(Steerable<T> owner, float radius) {
    this(owner, null, radius);
  }

  /**
   * Creates a {@code Flee} behavior for the specified owner and target.
   *
   * @param owner the owner of this behavior
   * @param target the target agent of this behavior.
   * @param radius
   */
  public FleeIfWithin(Steerable<T> owner, Steerable<T> target, float radius) {
    super(owner, target);
    this.radius = radius;
  }

  @Override
  protected SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
    // We just do the opposite of seek, i.e. (owner.getPosition() - target.getPosition())
    // instead of (target.getPosition() - owner.getPosition())
    float dst2 = target.getPosition().dst2(owner.getPosition());
    if (dst2 < radius * radius) {
      steering.linear.set(owner.getPosition()).sub(target.getPosition()).nor().scl(getActualLimiter().getMaxLinearAcceleration());
    }

    // No angular acceleration
    steering.angular = 0;

    // Output steering acceleration
    return steering;
  }

  //
  // Setters overridden in order to fix the correct return type for chaining
  //
  @Override
  public FleeIfWithin<T> setOwner(Steerable<T> owner) {
    this.owner = owner;
    return this;
  }

  @Override
  public FleeIfWithin<T> setEnabled(boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  /**
   * Sets the limiter of this steering behavior. The given limiter must at least
   * take care of the maximum linear acceleration.
   *
   * @return this behavior for chaining.
   */
  @Override
  public FleeIfWithin<T> setLimiter(Limiter limiter) {
    this.limiter = limiter;
    return this;
  }

  @Override
  public FleeIfWithin<T> setTarget(Steerable<T> target) {
    this.target = target;
    return this;
  }

}
